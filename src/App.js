import React from "react";
import "./App.css";
import { BrowserRouter as Router, NavLink, Switch, Route } from "react-router-dom";

import Usuarios from "./components/Usuarios/Usuarios";
import AdicionarUsuario from "./components/AdicionarUsuario/AdicionarUsuario";
import Home from "./components/Home/Home";
import DetalhesUsuario from "./components/DetalhesUsuario/DetalhesUsuario";

function App() {
  return (
    <Router>
      <div className="App">
        <header>
          <nav>
            <ul>
              <li>
                <NavLink to="/" exact>
                  Início
                </NavLink>
              </li>
              <li>
                <NavLink to="/usuarios">Usuários Cadastrados</NavLink>
              </li>
              <li>
                <NavLink to="/adicionar">Adicionar Usuários</NavLink>
              </li>
            </ul>
          </nav>
        </header>
        <main>
          <Switch>
            <Route path="/usuarios/:id">
              <DetalhesUsuario />
            </Route>
            <Route path="/usuarios">
              <Usuarios />
            </Route>
            <Route path="/adicionar">
              <AdicionarUsuario></AdicionarUsuario>
            </Route>
            <Route path="/" exact>
              <Home></Home>
            </Route>
            <Route path="">
              <PaginaNaoEncontrada></PaginaNaoEncontrada>
            </Route>
          </Switch>
        </main>
      </div>
    </Router>
  );
}

function PaginaNaoEncontrada() {
  const home = {
    margin: "2rem 0",
    padding: "1.25rem",
    boxShadow: "0 2px 5px rgba(0, 0, 0, 0.14)",
    height: "100%",
    textAlign: "center",
  };

  const h1 = {
    fontSize: "1.25rem",
    textTransform: "uppercase",
    color: "#ca2c68",
    margin: "0.25rem 0",
  };

  return (
    <div style={home}>
      <h1 style={h1}>404</h1>
      <br/>
      <span>Página não encontrada</span>
    </div>
  );
}

export default App;
