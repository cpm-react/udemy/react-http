import React from "react";
import { Link } from "react-router-dom";

import "./Usuario.css";

function Usuario(props) {
  return (
    <div className="Usuario">
      <Link to={`/usuarios/${props.usuario.id}`}>
        <ul>
          <li>
            <strong>ID:</strong> {props.usuario.id}
          </li>
          <li>
            <strong>Nome:</strong> {props.usuario.nome}{" "}
            {props.usuario.sobrenome}
          </li>
          <li>
            <strong>Email:</strong> {props.usuario.email}
          </li>
        </ul>
      </Link>
      <button onClick={props.removerUsuario}>&times;</button>
    </div>
  );
}

export default Usuario;
