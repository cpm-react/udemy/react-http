import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";

import "./DetalhesUsuario.css";

function DetalhesUsuario() {
  const { id } = useParams();

  const [usuario, setUsuario] = useState({});

  useEffect(() => {
    fetch(`https://reqres.in/api/users/${id}`)
      .then((resposta) => resposta.json())
      .then(function (dados) {
        if (dados.data) {
          setUsuario({
            id: dados.data.id,
            nome: dados.data.first_name,
            sobrenome: dados.data.last_name,
            email: dados.data.email,
            foto: dados.data.avatar,
          });
        }
      });
  }, [id]);

  if (usuario.nome !== undefined) {
    return (
      <div className="home">
        <h2>
          {usuario.nome} {usuario.sobrenome}
        </h2>
        <img src={usuario.foto} alt={usuario.nome} />
        <p>{usuario.email}</p>
        <Link to="/usuarios">Voltar</Link>
      </div>
    );
  }

  return (
    <div className="home">
      <h2>Usuário não encontrado</h2>
      <br/>
      <Link to="/usuarios">Voltar</Link>
    </div>
  );
}

export default DetalhesUsuario;
