import React from "react";

const home = {
    margin: '2rem 0',
    padding: '1.25rem',
    backgroundColor: '#ffebf3',
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.14)'
  }
  
const h1 = {
    fontSize: '1.25rem',
    textTransform: 'uppercase',
    color: '#ca2c68',
    margin: '0.25rem 0',
    textAlign: 'center'
  }

function Home() {
  return <div style={home}><h1 style={h1}>Início</h1></div>;
}

export default Home;
